<?php namespace DayToday\Wowapi;

use App;
use Config;
use \Illuminate\Console\Command;
use Illuminate\Support\ServiceProvider;

use DayToday\Wowapi\Commands;

class WowapiServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the service provider.
	 *
	 * @return void
	 */
	public function boot()
	{
		// Allows Laravel to properly load the views, configuration, and other resources for Wowapi.
		$this->package('daytoday/wowapi', 'daytoday/wowapi');

		// Load the commands related to wowapi
		// Artisan::add(new DayToday\Wowapi\Commands\InstallWowapiCommand);
		$this->commands(array(
			'wowapi.install',
			'wowapi.update',
			));

		// Load the routes for Wowapi.
		include __DIR__.'/../../routes.php';

		// Load the filters for Wowapi.
		include __DIR__.'/../../filters.php';
	}


	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerContent();
		$this->registerPost();
		$this->aliasWowapi(); // Uncomment this if using Wowapi without Laravel
		$this->registerInstallCommand();
		$this->registerUpdateCommand();
		$this->registerWowapi();


	}

	/**
	 * Alias Wowapi so that we can call it via the WowapiFacade.
	 * This is a laravel specific function.
	 *
	 * @return void
	 */
	private function aliasWowapi()
	{
		$this->app->booting(function()
		{
			$loader = \Illuminate\Foundation\AliasLoader::getInstance();
			$loader->alias('Wowapi', 'DayToday\Wowapi\Facades\WowapiFacade');
		});
	}

	private function registerInstallCommand()
	{
		$this->app['wowapi.install'] = $this->app->share(function($app)
		{
			return new Commands\InstallWowapiCommand();
		});
	}

	private function registerUpdateCommand()
	{
		$this->app['wowapi.update'] = $this->app->share(function($app)
		{
			return new Commands\UpdateWowapiCommand();
		});
	}

	/**
	 * Register the content provider based on the selected driver.
	 *
	 * @return void
	 */
	private function registerContent()
	{
		$this->app['wowapi.content'] = $this->app->share(function($app)
		{
			$content = $app['config']['daytoday/wowapi::contentDriver'];

			switch($content) {
				case 'html':
					return new Content\HtmlContent;
					break;
			}
			throw new \Exception("Invalid content driver [$content] selected.");
		});
	}

	private function registerPost()
	{
		$this->app->booting(function()
		{
			$loader = \Illuminate\Foundation\AliasLoader::getInstance();
			$loader->alias('Post', 'DayToday\Wowapi\Facades\PostFacade');
		});
		$this->app->bind(
			'DayToday\Wowapi\Repositories\PostRepository',
			'DayToday\Wowapi\Models\Post'
			);
		$this->app['wowapi.post'] = $this->app->share(function($app)
		{
			return new Models\Post;
		});
	}

	/**
	 * Register a Watho with each of its selected dependencies.
	 *
	 * @return void
	 */
	private function registerWowapi()
	{
		$this->app['wowapi'] = $this->app->share(function($app)
		{
			return new Wowapi(
				$app['wowapi.content']
			);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}