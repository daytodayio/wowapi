<?php namespace DayToday\Wowapi\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class UpdateWowapiCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'wowapi:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update all of the components needed for Wowapi to function.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		if($this->option('bench')) {
			$this->info('Publishing for workbench.' . PHP_EOL . '-------------------------');
		}

		// Do not run migrations by default
		if ($this->option('migrate')) { // Not Skipping
			if ($this->option('bench')) { // Benching
				$this->call('migrate', array( '--bench' => 'daytoday/wowapi'));
			} else { // Publishing
				$this->call('migrate', array('--package' => 'daytoday/wowapi'));
			}
		} else {
			$this->info('Skipping migrations.');
		}

		// Run assets by default
		if ($this->option('no-assets')) { // Skipping
			$this->info('Skipping publishing assets.');
		} else {
			if ($this->option('bench')) { // Benching
				$this->call('asset:publish', array( '--bench' => 'daytoday/wowapi'));
			} else { // Publishing
				$this->call('asset:publish', array('package' => 'daytoday/wowapi'));
			}
		}

		// Run views by default
		if ($this->option('no-views')) { // Skipping
			$this->info('Skipping publishing views.');
		} else {
			if ($this->option('bench')) { // Benching
				$this->call('view:publish', array('--path' => '/workbench/daytoday/wowapi/src/views/', 'package' => 'daytoday/wowapi'));
			} else { // Publishing
				$this->call('view:publish', array('package' => 'daytoday/wowapi'));
			}
		}

		// Run configs by default
		if ($this->option('no-configs')) { // Skipping
			$this->info('Skipping publishing configs.');
		} else {
			if ($this->option('bench')) { // Benching
				$this->call('config:publish', array('--path' => '/workbench/daytoday/wowapi/src/config/', 'package' => 'daytoday/wowapi'));
			} else { // Publishing
				$this->call('config:publish', array('package' => 'daytoday/wowapi'));
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('migrate', null, InputOption::VALUE_NONE, 'Run the migrations for Wowapi.', null),
			array('no-assets', null, InputOption::VALUE_NONE, 'Skip updating of the assets for Wowapi.', null),
			array('no-views', null, InputOption::VALUE_NONE, 'Skip updating of the views for Wowapi.', null),
			array('no-configs', null, InputOption::VALUE_NONE, 'Skip updating of the configs for Wowapi.', null),
			array('bench', null, InputOption::VALUE_NONE, 'Update the wowapi workbench; this is a convience method for development.', null),
		);
	}
}