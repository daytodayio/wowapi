<?php namespace DayToday\Wowapi\Commands;

use Log;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class InstallWowapiCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'wowapi:install';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Install all of the components needed for Wowapi to function.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		if($this->option('bench')) {
			Log::info('Installing Wowapi for workbench.');
		}

		// Run migrations by default
		if ($this->option('no-migrate')) {
			$this->info('Skipping migrations.');
		} else {
			if ($this->option('bench')) { // Benching
				$this->call('migrate', array( '--bench' => 'daytoday/wowapi'));
			} else { // Installing
				$this->call('migrate', array('--package' => 'daytoday/wowapi'));
			}
		}

		// Install assets by default
		if ($this->option('no-assets')) {
			$this->info('Skipping publishing assets.');
		} else {
			if ($this->option('bench')) { // Benching
				$this->call('asset:publish', array( '--bench' => 'daytoday/wowapi'));
			} else { // Installing
				$this->call('asset:publish', array('package' => 'daytoday/wowapi'));
			}
		}

		// Install views by default
		if ($this->option('no-views')) {
			$this->info('Skipping publishing views.');
		} else {
			if ($this->option('bench')) { // Benching
				$this->call('view:publish', array('--path' => '/workbench/daytoday/wowapi/src/views/', 'package' => 'daytoday/wowapi'));
			} else { // Installing
				$this->call('view:publish', array('package' => 'daytoday/wowapi'));
			}
		}

		// Install configs by default
		if ($this->option('no-configs')) {
			$this->info('Skipping publishing configs.');
		} else {
			if ($this->option('bench')) { // Benching
				$this->call('config:publish', array('--path' => '/workbench/daytoday/wowapi/src/config/', 'package' => 'daytoday/wowapi'));
			} else { // Installing
				$this->call('config:publish', array('package' => 'daytoday/wowapi'));
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('no-migrate', null, InputOption::VALUE_NONE, 'Skip installation of the migrations for Wowapi.', null),
			array('no-assets', null, InputOption::VALUE_NONE, 'Skip installation of the assets for Wowapi.', null),
			array('no-views', null, InputOption::VALUE_NONE, 'Skip installation of the views for Wowapi.', null),
			array('no-configs', null, InputOption::VALUE_NONE, 'Skip installation of the configs for Wowapi.', null),
			array('bench', null, InputOption::VALUE_NONE, 'Install the wowapi workbench; this is a convience method for development.', null),
		);
	}
}
