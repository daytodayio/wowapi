<?php namespace DayToday\Wowapi\Controllers;

use Input;
use DayToday\Wowapi\Repositories\PostRepository;
use Redirect;
use Response;
use Validator;
use View;
use Wowapi;

class PostController extends BaseController {
	
	protected $posts;

	public function __construct(PostRepository $posts)
	{
		parent::__construct();
		// Prevent us from just peeking at the ajax responses.
		$this->beforeFilter('@filterNonAjax');
		$this->posts = $posts;
	}

	public function filterNonAjax($route, $request) {
		if (!$request->ajax()) {
			return Redirect::to('wowapi/admin');
		}
	}

	public function index()
	{
		return $this->posts->getAll();
	}
	
	public function show($id)
	{
		return $this->posts->getById($id);
	}

	public function store()
	{

	}

	public function edit($id)
	{
		return $this->post->getById($id);
	}

	public function update($id)
	{
		if ($id === '0') { // This is a new post being saved.
			return Response::json(array(
				'id' => $this->posts->validate($id),
				));
		}
		return Input::all();
	}
}