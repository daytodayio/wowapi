<?php namespace DayToday\Wowapi\Controllers;

use Config;
use Controller;
use View;

class BaseController extends Controller {
	
	protected $theme;

	public function __construct()
	{
		$this->theme = Config::get('daytoday/wowapi::theme');

		// $this->setLayout();
	}

	protected function setLayout()
	{
		$this->layout = View::make('layouts.' . $this->theme);
	}

}