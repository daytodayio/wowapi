<?php namespace DayToday\Wowapi\Controllers;


use Auth;
use DayToday\Wowapi\Repositories\PostRepository;
use View;

class BlogController extends BaseController {	

	protected $posts;

	public function __construct(PostRepository $posts)
	{
		parent::__construct();

		// Kick the user to install Wowapi if they haven't already.
		$this->beforeFilter('wowapi.installed');

		// Set the correct PostRepo
		$this->posts = $posts;
	}

	public function index()
	{
		$posts = $this->posts->getAllActive();
		return View::make('daytoday/wowapi::posts.index', compact('posts'));
	}
	
	/**
	 * We still don't have a good way to let users know that a uri doesn't exist.
	 */
	public function show($id)
	{
		return View::make('daytoday/wowapi::posts.show', $this->compactLookup($id));
	}

	public function showBySlug($slug)
	{
		$post = $this->posts->getBySlug($slug);
		return View::make('daytoday/wowapi::posts.show', $this->compactLookup($post->id));
	}

	private function compactLookup($id)
	{
		$post = $this->posts->getById($id);
		$prevPost = $this->posts->getPreviousById($post->id);
		$nextPost = $this->posts->getNextById($post->id);
		$prevPostUrl = null;
		$nextPostUrl = null;
		if (!is_null($prevPost)) {
			$prevPostUrl = $prevPost->getUrl();
		}
		if (!is_null($nextPost)) {
			$nextPostUrl = $nextPost->getUrl();
		}
		return array(
			'post' 			=> $post,
			'prevPost' 	=> $prevPostUrl,
			'nextPost' 	=> $nextPostUrl,
			);
	}
}