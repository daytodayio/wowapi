<?php namespace DayToday\Wowapi\Controllers;

use Auth;
use Input;
use DayToday\Wowapi\Repositories\PostRepository;
use View;

class AdminController extends BaseController {	

	protected $posts;

	public function __construct(PostRepository $posts)
	{
		parent::__construct();

		$this->posts = $posts;
	}
	public function index()
	{
		$posts = $this->posts->getAll();
		return View::make('daytoday/wowapi::admin.index', compact('posts'));
	}

	public function show()
	{
		return View::make('daytoday/wowapi::admin.show');
	}

	public function edit($id)
	{
		$post = $this->posts->getById($id);
		return View::make('daytoday/wowapi::admin.edit', compact('post'));
	}

	public function save()
	{
		return Input::all();
	}
}