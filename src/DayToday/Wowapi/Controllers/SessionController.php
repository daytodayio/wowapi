<?php namespace DayToday\Wowapi\Controllers;


use Auth;
use Input;
use Redirect;
use Session;
use DayToday\Wowapi\Models\User;
use Validator;
use View;

/**
 * Handles all of the session actions.
 */
class SessionController extends BaseController {

	/**
	 * Show the login page to create a new session.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('daytoday/wowapi::admin.login');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$credentials = Input::only(array('email', 'password'));
		$remember = Input::get('remember', false);
		// Validation
		$validator = Validator::make($credentials, User::$authRules);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator);
		}

		// Authentication
		if (Auth::attempt($credentials, $remember)) {
			// Passed
			return Redirect::intended('wowapi/admin');
		} else {
			Session::flash('badCredentials', 'Invalid email address or password.');
			return Redirect::back();
		}
	}

	/**
	 * Destroy the current session.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();

		return Redirect::to('wowapi/login');
	}
}
