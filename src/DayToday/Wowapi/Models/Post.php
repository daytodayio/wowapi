<?php namespace DayToday\Wowapi\Models;

use Config;
use DayToday\Wowapi\Content\HtmlContent as Content;
use \Illuminate\Database\Eloquent\Model;
use DayToday\Wowapi\Repositories\PostRepository;

class Post extends Model implements PostRepository {

	protected $table = 'wowapi_posts';

	protected $fillable = array('title', 'subtitle', 'slug', 'active', 'published_at');

	protected static $date_format = 'Y-m-d H:i:s';

	protected static $const_rules = array(
		'title' 				=> 'required|between:1,255|regex:^[A-Za-z][A-Za-z0-9._^%$#!~@, -]*(?:_[A-Za-z0-9._^%$#!~@, -]+)*$|unique',
		'subtitle' 			=> 'between:1,255',
		'active'				=> 'required', // Can be false or true, but must be present.
		);

	protected static $publish_rules = array(
		'slug'					=> 'required|between:1,255|regex:^[a-z][a-z0-9]*(?:_[a-z0-9._^%$#!~@,-]+)*$|unique', // No CAPS
		'published_at'	=> 'date:Y-m-d H:i:s',
		);

	protected $content;

	// public function __construct(Content $content)
	// {
		// parent::__construct();
		// $this->content = $content;
	// }

	public function getRules()
	{
		return static::$rules;
	}

	public function getAll()
	{
		return Post::all();
	}

	public function getAllActive()
	{
		return Post::whereActive('1')->get();
	}

	public function getById($id)
	{
		return Post::find($id);
	}

	public function getUri()
	{
		return Config::get('daytoday/wowapi::uri') . '/' . $this->slug;
	}

	public function getUrl()
	{
		return url( Config::get('daytoday/wowapi::uri') . '/' . $this->slug);
	}
	
	public function getBySlug($slug)
	{
		return Post::where('slug', '=', $slug)->first();
	}

	public function getPreviousById($id)
	{
		return Post::where('id', '<', $id)->orderBy('id', 'desc')->first();
	}

	public function getNextById($id)
	{
		return Post::where('id', '>', $id)->orderBy('id', 'asc')->first();
	}

	// public function validate($input)
	// {
		// return $this->content->save('6');
	// }
}