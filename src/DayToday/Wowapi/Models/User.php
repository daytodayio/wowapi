<?php namespace DayToday\Wowapi\Models;

use \Illuminate\Database\Eloquent\Model;

class User extends Model {

	public static $authRules = array(
		'email' 			=> 'required|email',
		'password' 		=> 'required|min:8'
		);
	public static $rules = array(
		'email' 			=> 'required|email|unique:wowapi_users',
		'password' 		=> 'required|min:8',
		'first_name' 	=> 'sometimes|required|alpha_num',
		'last_name'		=>	'sometimes|alpha_num'
		);

	protected $guarded = array('id', 'password');

	protected $table = 'wowapi_users';
}