<?php namespace DayToday\Wowapi\Content;

abstract class Content {

	protected $content;

	/**
	 * Returns the content type.
	 *
	 * @return string
	 */
	protected function getContentType()
	{
		return $this->content;
	}

	/**
	 * Sets the content type.
	 *
	 * @return void
	 */
	protected function setContentType($content)
	{
		$this->content = $content;
	}
}