<?php namespace DayToday\Wowapi\Facades;

use Illuminate\Support\Facades\Facade;

class WowapiFacade extends Facade {
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'wowapi'; }
}