<?php namespace DayToday\Wowapi;

use Schema;
use DayToday\Wowapi\Models\Post;

class Wowapi {

	public $content;

	public function __construct(Content\Content $content)
	{
		$this->content = $content;
	}
	
	public function hello()
	{
		return 'Hello from Wowapi!';
	}

	public function isInstalled()
	{
		if(Schema::hasTable('wowapi')) {
			return true;
		}
		else {
			return false;
		}
	}

	public function getAllPosts()
	{
		return Post::all();
	}

	public function getPost($slug)
	{
		return Post::findSlug($slug);
	}

}