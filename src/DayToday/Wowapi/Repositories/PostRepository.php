<?php namespace DayToday\Wowapi\Repositories;

interface PostRepository {
	public function getAll();
	public function getAllActive();
	public function getById($id);
	public function getBySlug($slug);
	public function getPreviousById($id);
	public function getNextById($id);
	public function getRules();
}