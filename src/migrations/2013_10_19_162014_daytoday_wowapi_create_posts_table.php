<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DaytodayWowapiCreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wowapi_posts', function($table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('subtitle');
			$table->string('slug')->unique();
			$table->boolean('active')->default(0);
			$table->timestamp('published_at')->nullable()->default(null);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wowapi_posts');
	}

}