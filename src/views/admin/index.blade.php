@extends('daytoday/wowapi::layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-3">
			<div class="row">
				<div class="col-xs-6">
					<a href="{{ url('wowapi/admin/create') }}" class="btn btn-block btn-default">Create Post</a>
				</div>
			</div>
			<!-- <hr> -->
		</div>
		<div class="col-md-6">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Title</th>
						<th>Sub-Title</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($posts as $post)
						@if($post->active)
							<tr class="success">
						@else
							<tr>
						@endif
							<td>{{ $post->title }}</td>
							<td>{{ $post->subtitle }}</td>
							<td>{{ $post->active }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop