@extends('daytoday/wowapi::layouts.master')

@section('links')
	<link rel="stylesheet" type="text/css" href="{{ asset('/packages/daytoday/wowapi/js/components/pen/src/pen.css') }}">
@stop

@section('content')
	<div class="row">
		<div class="col-md-3">
			<div class="row">
				<div class="col-xs-6">
					<a href="#" class="btn btn-block btn-default" id="save-draft">Update Post</a>
				</div>
				<div class="col-xs-6">
					<a href="#" class="btn btn-block btn-default" id="publish" data-id="{{ $post->id }}" data-active="{{ $post->active }}">
					@if($post->active)
						Unpublish
					@else
						Publish
					@endif
					</a>
				</div>
			</div>
			<hr>
		</div>
		<div class="col-md-6">
			<h1 id="post-title" contenteditable="true">{{ $post->title }}</h1>
			<h2 id="post-subtitle" contenteditable="true">{{ $post->subtitle }}</h2>
			<div id="post-content">
				<p>Just start typing...</p>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script src="{{ asset('/packages/daytoday/wowapi/js/components/pen/src/pen.js') }}"></script>
	<script src="{{ asset('/packages/daytoday/wowapi/js/components/pen/src/markdown.js') }}"></script>
	<script>
		!(function($) {
			var publishButton = $('#publish');
			var postTitle = $('#post-title');
			var postSubtitle = $('#post-subtitle');
			var editor = $('#post-content');
			var pen = new Pen('#post-content');

			function generateUrlPath() {
				var pathArray = window.location.pathname.split( '/' );
				var newPathname = "/";
				for ( i = 0; i < pathArray.length - 3; i++ ) {
				  newPathname += pathArray[i];
				}
				newPathname += "/posts/" + publishButton.data('id');
				var finishedPathname = window.location.protocol + "//" + window.location.host + newPathname;
				return finishedPathname;
			}


			function collectPostData () {
				return {
					title: postTitle.text().trim(),
					subtitle: postSubtitle.text().trim(),
					content: editor.html().trim(),
					};
			}

			function saveDraft (response, button) {
				var originalText = button.text();
					var originalWidth = button.width;
					button.text('Saved!')
						.removeClass('btn-default')
						.addClass('btn-success');
					setTimeout(function () {
						button.text(originalText)
							.removeClass('btn-success')
							.addClass('btn-default');
					}, 2800);
			}
			$('#save-draft').click(function (event) {
				var button = $(event.target);
				$.ajax({
					url: generateUrlPath(),
					method: 'PUT',
					data: collectPostData()
				}).success( function (response) {
					saveDraft(response, button);
				}).fail(function (response) {
					console.warn('Something went wrong here...');
					console.warn(response);
				});
			});
		}(jQuery));
	</script>
@stop