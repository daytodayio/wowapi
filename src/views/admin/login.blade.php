@extends('daytoday/wowapi::layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h1>Login</h1>
			{{ Form::open(array('url' => 'wowapi/login', 'role' => 'form'))}}
			  <div class="form-group">
			    <label for="email">Email address</label>
			    @if($errors->has('email'))
			    	<p class="help-block text-warning">{{ $errors->first('email') }}</p>
			    @endif
			    @if(Session::has('badCredentials'))
			    	<p class="help-block text-warning">{{ Session::get('badCredentials') }}</p>
			    @endif
			    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
			  </div>
			  <div class="form-group">
			    <label for="password">Password</label>
			    @if($errors->has('password'))
			    	<p class="help-block text-warning">{{ $errors->first('password') }}</p>
			    @endif
			    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
			  </div>
			  <div class="checkbox">
  			  <label for="remember">
  			    <input type="checkbox" id="remember" name="remember" style="position:relative;bottom:2px;"> Remember Me
  			  </label>
  			</div>
			  <button type="submit" class="btn btn-default">Submit</button>
			{{ Form::close() }}
		</div>
	</div>
@stop