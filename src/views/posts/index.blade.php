@extends('daytoday/wowapi::layouts.postMaster')

@section('page-title')
Posts
@stop

@section('content')
	<div class="row">
		<div class="col-md-3">
			<h4>Tags</h4>
			<hr>
			<h5><a href="#">Future Tags</a></h5>
		</div>
		<div class="col-md-9">
			@foreach( $posts as $post )
				<div class="row">
					<div class="col-md-12">
						<h3>{{ $post->title }} <small>{{ $post->subtitle }}</small></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<a href="{{ $post->getUrl() }}">Read More...</a>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@stop