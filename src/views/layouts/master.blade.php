<!DOCTYPE html>
<html ng-app>
	<head>
		<!-- html5-boilerplate : A professional front-end template for building fast, robust, and adaptable web apps or sites. -->
		<!-- http://html5boilerplate.com/ -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title></title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" type="text/css" href="{{ asset('/packages/daytoday/wowapi/' . 'yeti.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('/packages/daytoday/wowapi/' . 'author.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('/packages/daytoday/wowapi/themes/' . Config::get('daytoday/wowapi::theme') . '.css') }}">
		@yield('links')
		<meta name="author" content="">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">

	</head>
	<body>
		<header>
			@include('daytoday/wowapi::layouts.header')
		</header>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					@yield('content')
				</div>
			</div>
		</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
		<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
		<script src="{{ asset('/packages/daytoday/wowapi/js/components/instantClick/instantClick.min.js') }}"></script>
		@yield('scripts')
	</body>
</html>