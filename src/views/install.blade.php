@extends('daytoday/wowapi::layouts.master')

@section('content')
<div class="row">
	<div class="col-md-12 text-center">
		<h1>Thanks for adding Wowapi to your project!</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12 text-center">
		<p>Simply click the install below to prep your project to use Wowapi.</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12 text-center">
		<a href="{{ url('wowapi/install') }}" class="btn btn-primary btn-lg">Install</a>
	</div>
</div>
@stop