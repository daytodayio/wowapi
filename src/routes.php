<?php

use DayToday\Wowapi\Models\Post;

/*
 * Here are all of the administrative routes for managing wowapi.
 */
Route::group(array('prefix' => 'wowapi'), function()
{
	// For those who just hit /wowapi
	Route::get('/', function() { return Redirect::to('wowapi/admin'); });

	// Sessions (i.e. login and logout at this point)
	Route::get('login', array('as' => 'login', 'uses' => 'DayToday\Wowapi\Controllers\SessionController@create'));
	Route::post('login', array('uses' => 'DayToday\Wowapi\Controllers\SessionController@store'));
	Route::get('logout', array('as' => 'login', 'uses' => 'DayToday\Wowapi\Controllers\SessionController@destroy'));
	
	// One-click Install
	Route::get('install', function() {
		if(!Wowapi::isInstalled())
			// If you are developing Wowapi from within the workbench folder, uncomment below.
			Artisan::call('wowapi:install'); // , array( '--bench' => 'daytoday/wowapi'));
		return Redirect::to('wowapi');
	});
	
	// Actual admin stuffs
	Route::group(array('before' => 'wowapi.auth.admin'), function()
	{
		// Admin dashboard
		Route::get('admin', 'DayToday\Wowapi\Controllers\AdminController@index');
		// Admin page to create a new post
		Route::get('admin/create', 'DayToday\Wowapi\Controllers\AdminController@show');
		// Route to handle updating/creating a new Post from the route above.
		Route::post('admin/create', 'DayToday\Wowapi\Controllers\AdminController@save');
		// Admin page to edit an existing post
		Route::get('admin/edit/{postId}', 'DayToday\Wowapi\Controllers\AdminController@edit');
		// Handle all of the validation and RESTfulness for the Post model.
		Route::resource('posts', 'DayToday\Wowapi\Controllers\PostController');
	});
});


/*
 * Here are all of the front facing routes for the blog.
 */
Route::resource(Config::get('daytoday/wowapi::uri'), 'DayToday\Wowapi\Controllers\BlogController', array('except' => array('show')));
Route::get(Config::get('daytoday/wowapi::uri') . '/{id}', 'DayToday\Wowapi\Controllers\BlogController@show')->where('id', '\d+');
Route::get(Config::get('daytoday/wowapi::uri') . '/{slug}', 'DayToday\Wowapi\Controllers\BlogController@showBySlug')->where('slug', '^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$');
/* Yep, that's it! */

// Route::group(array('prefix' => Config::get('daytoday/wowapi::uri')), function()
// {

	// Public index
	// Route::get('/', function()
	// {
		// if(Wowapi::isInstalled()) {
			// return View::make('daytoday/wowapi::posts.index');
		// }
		// else {
			// return View::make('daytoday/wowapi::install');
		// }
	// });
// });