<?php

return array(

	/*
	 |--------------------------------------------------------------------------
	 | Content Driver
	 |--------------------------------------------------------------------------
	 |
	 | This options controls the content driver used for parsing content files.
	 | Currently supported drivers include: 'html'
	 | Future drivers include: 'markdown' and 'plain'
	 |
	 */

	'contentDriver' => 'html',

	/*
	 |--------------------------------------------------------------------------
	 | Base Slug
	 |--------------------------------------------------------------------------
	 |
	 | Here you may set the base slug for all of your content.
	 |
	 | e.g., domain.com/{baseSlug}/individual-slug
	 */

	'uri' => 'blog',

	/*
	 |--------------------------------------------------------------------------
	 | Theme Selection
	 |--------------------------------------------------------------------------
	 |
	 | Here you may select a theme to be used throughout your content. Themes
	 | are published to your application's public folder under
	 | public/packages/daytoday/wowapi/themes.
	 |
	 */

	'theme'	=> 'light',

	/*
	 |--------------------------------------------------------------------------
	 | Secure URLs
	 |--------------------------------------------------------------------------
	 |
	 | This options controls secure url generation and viewing. https will be
	 | used when this is set to true.
	 |
	 */

	'secure' => false,
	
);