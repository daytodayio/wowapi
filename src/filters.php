<?php

Route::filter('wowapi.auth', function()
{
	if (Auth::guest()) return Redirect::guest('wowapi/login');
});

Route::filter('wowapi.auth.admin', function()
{
	if (Auth::guest() or !Auth::user()->admin) return Redirect::guest('wowapi/login');
});

Route::filter('wowapi.installed', function()
{
	if(!Wowapi::isInstalled()) {
		return Redirect::to('wowapi/install');
	}
});