# Wowapi
Wowapi is a Micro Content Management System (CMS) built ontop of [Laravel 4](http://laravel.com).

[![wercker status](https://app.wercker.com/status/2d02d1e664c18f4cfc73c5492b208264/m "wercker status")](https://app.wercker.com/project/bykey/2d02d1e664c18f4cfc73c5492b208264)

## Installation

Update your composer.json file to include:

```
"require": {
	...
	"daytoday/wowapi": "dev-master"
},
"repositories": [
	{
		"type": "vcs",
		"url": "git@bitbucket.org:daytodayio/wowapi.git"
	}
],
```

update application config for auth.

Run `composer update`.

Add `'DayToday\Wowapi\WowapiServiceProvider'` to the list of service providers in `app/config/app.php`.

Run `php artisan migrate --package=daytoday/wowapi` or create your own custom tables.

Run `php artisan asset:publish daytoday/wowapi` to publish the necessary assets.

Run `php artisan view:publish daytoday/wowapi` to publish the necessary views.

If you want to adjust Wowapi's default configuration settings, run `php artisan config:publish daytoday/wowapi`.
This will push all of the configuration files to `app/config/packages/daytoday/wowapi` for editting.
